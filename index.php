<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Дизайн студия</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- Custom CSS -->
    <link href="css/lightbox.css" rel="stylesheet">
    <link href="css/hover-min.css" rel="stylesheet">
    <link href="css/shake.css" rel="stylesheet">
    <link href="css/agency.css" rel="stylesheet">

    <script src="js/jquery-1.8.2.min.js"></script>
    <script src="js/countdown.js"></script>
    <script src="js/countdown_config.js"></script>
    <script src="js/countdown_config2.js"></script>
    <script src="js/countdown_config3.js"></script>
      
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
<div class="overlay" title="окно"></div>
<div class="popup">
    <div class="close_window">x</div>
    <p style="    font-size: 20px;  color:black;  text-align: center;">Введите ваше имя и телефон и мы с вами свяжемся!</p>
    <form action="call.php" method="post">
        <input type="text" name="call_name" placeholder="Ваше имя" id="call_name" class="call_t">
        <input type="text" name="call_phone" placeholder="Ваш телефон" id="call_phone" class="call_t">
        <input type="button" value="Жду звонка!"  id="submit_call" class="cnt_3_2_1_btn" name="send">
    </form>
</div>

    <img src="img/phone3.png" class="phone_ring shake shake-constant hover-stop shake-slow">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Навигация</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#services">Услуги</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#portfolio">Наши работы</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#works">Как мы работаем</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#team">О нас</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Контакты</a>
                        </li>
                        <li>
                            <a class="page-scroll" id="zazak_header">Заказать звонок</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <!-- Header -->
        <header>
            <div class="container">
                <div class="intro-text">
                    <div class="intro-lead-in">
                        <div class="text-logo-left">Kuandykova Gulnar<br />Design studio</div>
                        <!--<img src="img/logos/sapfiras.png" class="logos">
                        <img src="img/gear.png" class="logos">-->
                        <div style="    display: inline-block;    background-color: white;    width: 2px;    height: 50px;    margin: 0px 10px;"></div>
                        <div class="text-logo-right">Anelya Alibay<br />Design studio</div>
                    </div>
                    <!--<div class="intro-lead-in"></div>-->
                    <div class="intro-heading">Дизайн интерьера</div>
                    <!--<a href="#services" class="page-scroll btn btn-xl">Tell Me More</a>-->
                </div>
            </div>
        </header>

        <!-- Services Section -->
        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Мы предлагаем</h2>
                        <h3 class="section-subheading text-muted">С нами вы можете получить:</h3>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/Home.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Дизайн интерьера жилых и общественных помещении</h4>                    
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/proektirovanie.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Проектирование жилых и общественных здании</h4>                    
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/hotel.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Дизайн кафе, ресторанов и отелей под ключ </h4>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/mebel.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Мебель из Европы Италия, Испания</h4>                    
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/santekhnika.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Сантехника из Европы</h4>                    
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/coffee.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Оборудование для кухни кафе и ресторанов</h4>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/shop.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Проектирование и комплексное оснащение ресторанов, кафе, магазинов</h4>                    
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/cleaning.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Клининговые услуги</h4>                    
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <img src="img/europe.png" class="icons hvr-buzz-out">
                        </span>
                        <h4 class="service-heading">Сопровождение заказчика с целью закупа мебели в Европу, Китай </h4>
                    </div>
                </div>
            </div>
        </section>
    <!--            Скидка        -->
    <div class="container centerBg" id="oformitZakaz">
        <div class="allBg">
            <div class="container allContainerBg headerContBg">
                <div class="row" style="margin: -22px 0px 0px 0px;   vertical-align: top;">
                    <div class="span8 timerLeftCentr">
                        <div style="margin: 15px 7px 0 0;">
                            <h1 class="inlineTable cGray timerLeftCentr2" style="font-size:50px">СКИДКА </h1><h1 class="inlineTable cRed" style="font-family: comRegular; vertical-align: top; font-size: 72px; margin: 10px -1px 0px 1px;font-weight:bold">10%</h1>
                            <div style="margin: 5px -5px 0px 0px;">
                                <h3 style="font-size: 24px; font-family: conRegular; margin:0;font-weight:bold" class="inlineBlockI cGray">НА</h3>
                                <h3 style="margin:0;font-size: 48px; font-family: conRegular; margin: 0px -6px;font-weight:bold" class="cBlue inlineBlockI">ПЕРВЫЙ</h3>
                                <h3 style="font-size: 24px; font-family: conRegular; margin:0; font-weight: bold;" class="cGray inlineBlockI">ЗАКАЗ</h3>
                            </div>
                            <h3 style="margin:-11px 0 0 0; font-size: 24px; font-family: conRegular;font-weight:bold" class="cGray">ДИЗАЙН-ПРОЕКТА</h3>

                            <h4 class="cGray size12" style="font-size: 18px; font-family: conMedium; margin: 0;">ПРИ ЗАКАЗЕ ЧЕРЕЗ САЙТ</h4>

                            <p class="cGray" style="font-family: conRegular; font-size: 18px; margin: 5px 0px 10px;">до конца акции:</p>
                            <div class="timer" style="float: right; margin: -4px -7px 0px 0px;">
                                <div class="container">
                                    <div class="flip day1Play"></div>
                                    <div class="flip dayPlay">
                                        <em style="color: black;">дней</em>
                                    </div>
                                    <div class="flip hour2Play"></div>
                                    <div class="flip hourPlay">
                                        <em style="color: black;">часов</em>
                                    </div>
                                    <div class="flip minute6Play"></div>
                                    <div class="flip minutePlay">
                                        <em style="color: black;">минут</em>
                                    </div>
                                    <div class="flip second6Play"></div>
                                    <div class="flip secondPlay">
                                        <em style="color: black;">секунд</em>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span4" style="margin: 35px 0 0 10px;width: 26%;display: inline-block;">
                        <div class="formLeftCentr">
                            <form method="post" action="call2.php" style="margin:0" name="Cons" onsubmit="return validateForm2()">
                            <div>
                                <h2 class="inlineBlockI cGray" style="margin: 7px -5px -8px 0px;"><strong style="font-size: 36px; font-family: conMedium; ">ОСТАВЬТЕ ЗАЯВКУ</strong></h2>
                                <h2 class="inlineBlockI cGray"  style="font-size: 36px; margin: 5px 0px 14px 2px;"><strong>НА РАСЧЕТ</strong></h2>
                            </div>
                            <h3 class="designProjectText1">дизайн-проекта </h3> &nbsp &nbsp <h3 class="designProjectText1-1">со скидкой</h3>
                            <div class="inputCenterStyle">
                                <img alt="" src="img/userB.png" class="inlineTable" style="vertical-align:middle;">
                                <input  name="name" class=" inlineTable" type="text" style="margin:0px 0 0 0; font-size: 16px; font-family: conRegular;" id="salename" onfocus="this.style.color='#000'; this.value='';" value="" placeholder="ваше имя*">
                            </div>
                            <div class="inputCenterStyle">
                                <img alt="" src="img/phoneB.png" class="inlineTable" style="vertical-align:middle;">
                                <input  name="phone" class=" inlineTable" type="tel" style="margin:0px 0 0 0; font-size: 16px; font-family: conRegular;     color: #252525;" id="salephone" onkeyup="formattingNumbers2( this )" onfocus="myFunctionCentr()" value="" placeholder="ваш телефон*">
                            </div>
                            <div class="inputCenterStyle">
                                <img alt="" src="img/mailB.png" class="inlineTable" style="vertical-align:middle;">
                                <input  class="inlineTable" type="email" id="saleemail" name="email" style="margin:0px 0 0 0; font-size: 16px; font-family: conRegular;" onfocus="this.style.color='#000'; this.value='';" value="" placeholder="ваш e-mail">
                            </div>
                            <input type="submit" id="sbmBtnCntr" class=" width229 submitBtnCentr" value="ОСТАВИТЬ ЗАЯВКУ">
                            </form>
                            <div class="textAlignCenter" style="margin:0px 0 0 0;  padding:0;">
                                <p class="textRightGarantiya cGray">Мы гарантируем</p>
                                <p class="textRightGarantiya cGray">конфидентиальность Ваших даных</p>
                            </div>
                        </div>
                        <script>// onkeyup="formattingNumbers( this )" value="+ 7 "
                        function myFunctionCentr()
                        {
                            document.forms["Cons"]["Cons[phone]"].value = "+7 ";
                        }
                        function validateForm2() {
                            var x = document.forms["Cons"]["Cons[phone]"].value.length;
                            if (x!=17) {
                                alert("Номер должен содержать 11 цифр!");
                                return false;
                            }
                            var y = document.forms["Cons"]["Cons[name]"].value.length;
                            var q = document.forms["Cons"]["Cons[email]"].value.length;
                            if (x!=17) {
                                alert("Номер должен содержать 11 цифр!");
                                return false;
                            }

                            if (y=="") {
                                alert("Вы не заполнили имя!");
                                return false;
                            }
                            if (q=="") {
                                alert("Вы не заполнили email!");
                                return false;
                            }
                        }
                        function formattingNumbers2( elem ) 
                        {
                            var pattern = '+ 7 123 456-78-90', arr = elem.value.match( /\d/g ), i = 0;
                            if ( arr === null ) return;
                            elem.value = pattern.replace( /\d/g, function( a, b ) {
                                if ( arr.length ) i = b + 1;
                                return arr.shift();
                            }).substring( 0, i );
                        }
                        </script>
                    </div>
                </div>
            </div>
        </div>  
    </div>



    <!-- 
    <section id="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Узнайте как наши дизайнеры могут преобразовать вашу квартиру</h2>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-6">
                    <h4 class="service-heading">ДО</h4>                    
                    <img src="img/before.jpeg" class="before_after_img">
                </div>
                <div class="col-md-6">
                    <h4 class="service-heading">ПОСЛЕ</h4>                    
                    <img src="img/after.jpeg" class="before_after_img">
                </div>
            </div>
        </div>
    </section>
Services Section -->
    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Наши работы</h2>
                    <h3 class="section-subheading text-muted">Работы сделавшие сотню людей счастливыми</h3>
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Wok to walk</h4>
                        <p class="text-muted">быстрая азиатская еда на ремизовка</p>
                    </div>
                </div>

                
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/worktime.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Рабочие моменты</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Дизайн холла Патифон/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Дизайн холла Патифон</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <!--<div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal7" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/ЖК Тау Шатыры в процессе/8.jpeg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>ЖК Тау Шатыры</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal8" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Квартира Толе Би Тау в процессе/3.jpeg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Квартира Толе Би Тау</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>-->
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal9" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Офис 3d визуализация/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Офис 3d</h4>
                        <p class="text-muted">визуализация</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal10" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Ресторан Лотос/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Ресторан Лотос</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal11" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Ресторан Лофт алматы/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Ресторан Лофт</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal12" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Салон Красоты Мокка г Астана/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Салон Красоты Мокка </h4>
                        <p class="text-muted">г. Астана</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal13" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/ТАУ САМАЛ/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>ТАУ САМАЛ</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal15" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Офис</h4>
                        <p class="text-muted">Курмангазы Панфилова</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal16" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/gulnara/Загородный дом/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Загородный дом</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal17" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/gulnara/Нурлы-Тау/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Нурлы-Тау</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal18" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Нурлы-Тау</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal19" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/ЖК РАДУГА/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>ЖК Радуга</h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/Аксайское ущелье дома отдыха/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Аксайское ущелье</h4>
                        <p class="text-muted">дома отдыха</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/anel/алатау престиж алматы/logo.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Алатау престиж </h4>
                        <p class="text-muted">г. Алматы</p>
                    </div>
                </div>

                

            </div>
        </div>
    </section>

    <!-- Why we
    <section id="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Почему нас выбирают?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="" class="portfolio-link" data-toggle="modal">
                        <img src="img/portfolio/anel/Restoran г Петропавловск/1.jpeg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Текст</h4>
                        <p class="text-muted"></p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="" class="portfolio-link" data-toggle="modal">
                        <img src="img/portfolio/anel/Restoran г Петропавловск/1.jpeg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Текст</h4>
                        <p class="text-muted"></p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="" class="portfolio-link" data-toggle="modal">
                        <img src="img/portfolio/anel/Restoran г Петропавловск/1.jpeg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Текст</h4>
                        <p class="text-muted"></p>
                    </div>
                </div>

            </div>
        </div>
    </section>
 -->
    <!-- Широкий и тд -->
    <section id="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Широкий ассортимент товаров из Италии и Испании</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/alma light 13 (cat BAJA).pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/alma-light-13-(cat-BAJA).png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Alma light</h4>
                        <a href="pdf/alma light 13 (cat BAJA).pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/CATALOGO_2014_COLCHONES_OK[1].pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/CATALOGO_2014_COLCHONES_OK[1].png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Catalogo 2014 Colchones</h4>
                        <a href="pdf/CATALOGO_2014_COLCHONES_OK[1].pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/CATALOGO_GENERAL_SILLONES_V3_OK.pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/CATALOGO_GENERAL_SILLONES_V3_OK.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Catalogo general sillones</h4>
                        <a href="pdf/CATALOGO_GENERAL_SILLONES_V3_OK.pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/Catalogos Dormitorios Modus Home.pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/Catalogos-Dormitorios-Modus-Home.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Catalogos Dormitorios Modus Home</h4>
                        <a href="pdf/Catalogos Dormitorios Modus Home.pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/Lighting Cat 2012 Mariner.pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/Lighting-Cat-2012-Mariner.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Lighting Cat 2012 Mariner</h4>
                        <a href="pdf/Lighting Cat 2012 Mariner.pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/ModusNatura2014.pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/ModusNatura2014.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Modus Natura 2014</h4>
                        <a href="pdf/ModusNatura2014.pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div><!--
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/PDF CATALOGO VALENTI EVOLUTION 2013.pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/CATALOGO-VALENTI-EVOLUTION-2013.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Catalogo Valenti Evolution 2013</h4>
                        <a href="pdf/PDF CATALOGO VALENTI EVOLUTION 2013.pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 portfolio-item">
                    <a href="pdf/Tecni Nova Glamour.pdf" class="portfolio-link" target="_blank">
                        <img src="img/pdf/logo_Tecni Nova Glamour.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4 style="     color: white;">Tecni Nova Glamour</h4>
                        <a href="pdf/Tecni Nova Glamour.pdf" class="portfolio-link" target="_blank"><p class="text-muted">Скачать</p></a>
                    </div>
                </div>-->
            </div>
        </div>
    </section>

    <!-- Partners -->
    <aside class="clients">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Наши партнеры</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-6">
                    <a href="">
                        <img src="img/mail/Logos Brands/Evroplast.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="">
                        <img src="img/mail/Logos Brands/image-25-07-15-18-31.jpeg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="">
                        <img src="img/logos/Logo Happy Home interiors.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <a href="">
                        <img src="img/mail/Logos Brands/лоймина_рестайлинг лого.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="">
                        <img src="img/mail/Logos Brands/GIRALOGO.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>


        <!-- Works  -->
    

    <section id="works">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Как мы работаем</h2>
                </div>
            </div>
            <div class="row viewchar">
                <div class="col-lg-3 text-center">
                    <figure class="chart" data-percent="19">
                        <figcaption>КОНСУЛЬТАЦИЯ</figcaption>
                        <svg width="200" height="200">
                          <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                        </svg>
                    </figure>
                </div>
                <div class="col-lg-9 rightnaw">
                    Выясняем ваши предпочтения, уточняем детали, определяем назначение и стиль интерьера
                </div>
            </div><div class="row viewchar">
                <div class="col-lg-3 text-center">
                    <figure class="chart" data-percent="25">
                        <figcaption>ЗАКЛЮЧЕНИЕ ДОГОВОРА</figcaption>
                        <svg width="200" height="200">
                          <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                        </svg>
                    </figure>
                </div>
                <div class="col-lg-9 rightnaw">
                    Планируем бюджет проекта, оформляем договор
                </div>
            </div><div class="row viewchar">
                <div class="col-lg-3 text-center">
                    <figure class="chart" data-percent="56">
                        <figcaption>ПЛАНИРОВАНИЕ</figcaption>
                        <svg width="200" height="200">
                          <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                        </svg>
                    </figure>
                </div>
                <div class="col-lg-9  rightnaw">
                    Разрабатываем концепцию, подбираем предметы интерьера на основании ваших вкусов и модных тенденций
                </div>
            </div><div class="row viewchar">
                <div class="col-lg-3 text-center">
                    <figure class="chart" data-percent="84">
                        <figcaption>ВИЗУАЛИЗАЦИЯ</figcaption>
                        <svg width="200" height="200">
                          <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                        </svg>
                    </figure>
                </div>
                <div class="col-lg-9 rightnaw">
                    Создаём подробную 3D-модель вашего интерьера и утверждаем планы помещений
                </div>
            </div><div class="row viewchar">
                <div class="col-lg-3 text-center">
                    <figure class="chart" data-percent="92">
                        <figcaption>АВТОРСКИЙ<br />НАДЗОР</figcaption>
                        <svg width="200" height="200">
                          <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                        </svg>
                    </figure>
                </div>
                <div class="col-lg-9 rightnaw">
                    Контролируем реализацию разработанных идей
                </div>
            </div><div class="row viewchar" >                                 
                        
                <div class="col-lg-3 text-center">
                    <figure class="chart" data-percent="100">
                        <figcaption>ФИНАЛ</figcaption>
                        <svg width="200" height="200">
                          <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                        </svg>
                    </figure>
                </div>
                <div class="col-lg-9 rightnaw">
                    Вы становитесь счастливым владельцем уютного и эксклюзивного интерьера
                </div>        
                        
                        
            </div>
        </div>
    </div>

    <!-- About Section -->


    <!-- About Section -->

    <!-- Team Section -->
    <section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Наша команда</h2>
                    <h3 class="section-subheading text-muted">Мы с радостью примем любой ваш заказ!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="team-member">
                        <div class="class_bla"><img style="    margin: -20px 0 0 0px;" src="img/team/gulnara.jpg" class="img-responsive img-circle" alt=""></div>
                        <h4>Гульнара Куандыкова</h4>
                        <p class="">Директор<br /></p>
                        <p class="">
                        Дизайнер интерьера и Декоратор <br />
                        <!--Руководитель студии Сапфир-АС<br />-->
                        Образование: технолог дизайнер<br />
						За 10 лет на рынке мною<br />
						разработаны более 50 частных и<br />
						общ. помещений
                        </p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="team-member">
                        <div class="class_bla"><img style="    margin: -20px 0 0 0px;" src="img/team/5.jpg" class="img-responsive img-circle" alt=""></div>
                        <h4>Анеля Алибай</h4>
                        <p class="">Директор<br /></p><p>
                         Архитектор, дизайнер<br />
                        Закончила КазГАСА<br />
                        КазГУ<br />
                        POLI.design Consorzio del Politecnico di Milano <br />
                        Работала в Казаэропроект</p>
                        <ul class="list-inline social-buttons">
                            <li><a target="_blank" href="https://www.facebook.com/profile.php?id=100003483131250&fref=photo"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a target="_blank" href="https://instagram.com/anelyaalibayeva/"><i class="fa fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="team-member">
                        <div class="class_bla"><img src="img/team/ayana.jpg" class="img-responsive img-circle" alt=""></div>
                        <h4>Аяна Куандыкова</h4>
                        <p class="text-muted">ПР-менеджер</p>
                        <!--<ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>-->
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <div class="class_bla"><img src="img/team/2.JPG" class="img-responsive img-circle" alt=""></div>
                        <h4>Свирская Анна</h4>
                        <p class="text-muted">Проектировщик, стаж 15 лет</p>
                        <!--<ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>-->
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <div class="class_bla"><img src="img/team/3.jpg" class="img-responsive img-circle" alt=""></div>
                        <h4>Владимир Марыш</h4>
                        <p class="text-muted">КазГАСА. Архитектор, дизайнер интерьера</p>
                        <!--<ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>-->
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <div class="class_bla"><img src="img/team/4.JPG" class="img-responsive img-circle" alt=""></div>
                        <h4>Алтынай Сатанова</h4>
                        <p class="text-muted">Дизайнер-фотограф</p>
                        <!--<ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p class="large text-muted"></p>
                </div>
            </div>
        </div>
    </section>

    <!-- About us in numbers -->
        <div class="">
            <div class="row">
                <div class="faktsBG allBg">                                    
                    <div class="container allContainerBg headerContBg">
                        <div style="margin-top:64px" class=" text-center">
                            <div class="inlineTable blocks">
                                <div class="numbers" id="num_1">0</div>
                                <div class="textNiz">ЛЕТ ОПЫТА</div>
                            </div>                            
                            <div class="inlineTable blocks">
                                <div class="numbers" id="num_2">0</div>
                                <div class="textNiz">ЧАСТНЫХ ПРОЕКТОВ В ГОД</div>
                            </div>
                            <div class="inlineTable blocks" style="margin: 5px 43px 20px -10px;">
                                <div class="numbers" id="num_3">0</div>
                                <div class="textNiz">ОБЩЕСТВЕННЫХ ПРОЕКТОВ</div>
                            </div>
                            <div class="inlineTable blocks"  style="">
                                <div class="numbers" id="num_4">0</div>
                                <div class="textNiz">ОФИСНЫХ ПОМЕЩЕНИЙ</div>
                            </div>
                            <div class="inlineTable blocks">
                                <div class="numbers" id="num_5">0</div>
                                <div class="textNiz">КЛИЕНТОВ К НАМ ОБРАТИЛИСЬ СНОВА</div>
                            </div>
                            <div class="inlineTable blocks"  style="margin-right: 34px;">
                                <div class="numbers" id="num_6">0</div>
                                <div class="textNiz">ПАРТНЕРОВ</div>
                            </div>
                            <div class="inlineTable blocks">
                                <div class="numbers" id="num_7">0</div>
                                <div class="textNiz">СТРАН ОБСЛУЖИВАНИЯ</div>
                            </div>
                            <div class="inlineTable blocks"  style="">
                                <div class="numbers" id="num_8">0</div>
                                <div class="textNiz">ГОРОДОВ</div>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>


    <!-- Отзывы
    <section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Отзывы</h2>
                    <h3 class="section-subheading text-muted"></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="otzyv">
                        <img src="img/male.png" class="img-responsive img-circle" alt="">
                        <div class="otz_v">
                            <h4>Марат</h4>
                            <p class="text-muted">Очень креативная студия! Заказывал проект с нуля от проектирования дома, дизайна и от закупа мебели с Европы! Все креативно, стильно, качественно!</p>                        
                        </div>
                    </div>

                    <div class="otzyv">
                    <img src="img/female.png" class="img-responsive img-circle" alt="">
                        <div class="otz_v">
                            <h4>Динара</h4>
                            <p class="text-muted">Благодарю вас за то, что вы воплотили мои мечты в реальность! Я действительно не ожидала что проект будет реализован в полной мере и то, что учтут все мои пожелания! </p>                        
                        </div>
                        
                        
                    </div>

                    <div class="otzyv">
                        <img src="img/female.png" class="img-responsive img-circle" alt="">
                        <div class="otz_v">
                            <h4>Марина</h4>
                            <p class="text-muted">Спасибо, Вам за вашу гибкость и умение бесконфликтно выходить  из любой ситуации! Желаем вашей команде дальнейших успехов и процветания!</p>                        
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>
     -->
    
    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Наши контактные данные</h2>
                    <h3 class="section-subheading text-muted">Сделайте заказ прямо сейчас!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="cnt_6_2_1">
                        <div class="cnt_6_2_1_1">Мы в социальных сетях</div>
                        <div class="cnt_6_2_1_2">
                            <a target="_blank" href="http://vk.com/id297311396"><img class="scl" src="img/vk-64.png" alt="vkontakte"></a>
                            <a target="_blank" href="https://www.facebook.com/profile.php?id=100009004909805&amp;fref=ts"><img class="scl" src="img/facebook-60.png" alt="facebook"></a>
                            <a target="_blank" href="https://instagram.com/taxikolesa_almaty/"><img class="scl" src="img/instagram-60.png" alt="instagram"></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">

                    <div class="zvonok_footer">Заказать звонок</div>
                    <div class="cnt_6_2_1_1">Адрес офиса: Бухар Жырау 28/оф.207  БЦ Evolution</div>  
                </div>
                <div class="col-lg-4">
                    <div class="cnt_6_2_1">
                        <div class="cnt_6_2_1_1">Звоните нам</div>
                        <div class="cnt_6_2_1_2">
                            <img src="img/WhatsApp-icon.png" alt="24 часа" class="hour24" style="    float: left;    margin: 0 -75px 0 30px;">
							<div>+7 (727) 317 01 95</div>
                            <div>+7 (707) 257 43 03</div>
                            <div>+7 (701) 851-19-04</div>
                            <div>+7 (727) 317-12-70</div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Ваше имя *" id="name" required data-validation-required-message="Пожалуйста введите ваше имя">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Ваша почта" id="email"  data-validation-required-message="Пожалуйста введите ваш email">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Ваш телефон *" id="phone" required data-validation-required-message="Пожалуйста введите ваш номер телефона">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Что вы хотели бы заказать?" id="message"  data-validation-required-message="Пожалуйста введите ваше пожелание"></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Отправить письмо</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>--><br />
            <div class="row">
                <div class="col-lg-12">
                    <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=NBM3Una4B6Re-4-Nm5FhqtJ7n_FSpyQ7&width=100%&height=450"></script>
                </div>
            </div>
        </div>
    </section>
    <!--
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Your Website 2014</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>-->

    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Restoran</h2>
                            <p class="item-intro text-muted">Описание</p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Restoran г Петропавловск/1.jpeg" data-lightbox="prt_1" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Restoran г Петропавловск/1.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Restoran г Петропавловск/2.jpeg" data-lightbox="prt_1" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Restoran г Петропавловск/2.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Restoran г Петропавловск/3.jpeg" data-lightbox="prt_1" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Restoran г Петропавловск/3.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Restoran г Петропавловск/4.jpeg" data-lightbox="prt_1" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Restoran г Петропавловск/4.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Restoran г Петропавловск/5.jpeg" data-lightbox="prt_1" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Restoran г Петропавловск/5.jpeg" alt="">
                                </a>
                                
                            </div>



                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Wok to walk</h2>
                            <p class="item-intro text-muted">Быстрая азиатская еда на ремизовка</p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/1.jpeg" data-lightbox="prt_2" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/1.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/2.jpeg" data-lightbox="prt_2" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/2.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/3.jpeg" data-lightbox="prt_2" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/3.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/4.jpeg" data-lightbox="prt_2" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/4.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/5.jpeg" data-lightbox="prt_2" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Wok to walk быстрая азиатская еда на ремизовка/5.jpeg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Аксайское ущелье дома отдыха</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/1.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/1.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/2.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/2.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/3.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/3.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/4.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/4.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/5.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/5.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/6.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/6.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/7.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/7.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/8.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/8.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/9.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/9.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/10.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/10.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/11.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/11.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/12.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/12.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/13.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/13.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/14.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/14.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/15.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/15.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Аксайское ущелье дома отдыха/16.jpeg" data-lightbox="prt_3" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Аксайское ущелье дома отдыха/16.jpeg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Алатау престиж</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/1.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/1.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/2.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/2.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/3.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/3.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/4.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/4.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/5.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/5.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/6.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/6.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/7.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/7.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/8.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/8.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/9.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/9.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/10.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/10.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/11.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/11.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/12.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/12.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/13.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/13.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/14.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/14.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/15.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/15.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/16.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/16.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/17.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/17.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/18.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/18.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/алатау престиж алматы/19.jpeg" data-lightbox="prt_4" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/алатау престиж алматы/19.jpeg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Рабочие моменты</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/ВОПРОС Рабочие моменты/1.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ВОПРОС Рабочие моменты/1.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ВОПРОС Рабочие моменты/2.png" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ВОПРОС Рабочие моменты/2.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ВОПРОС Рабочие моменты/3.png" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ВОПРОС Рабочие моменты/3.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/1.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/1.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/2.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/2.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/3.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/3.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/4.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/4.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/5.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/5.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/6.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/6.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/7.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/7.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/8.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/8.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/9.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/9.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/10.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/10.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Квартира Толе Би Тау в процессе/11.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Квартира Толе Би Тау в процессе/11.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/1.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/1.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/2.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/2.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/3.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/3.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/4.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/4.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/5.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/5.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/6.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/6.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/7.png" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/7.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/8.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/8.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/9.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/9.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК Тау Шатыры в процессе/10.jpeg" data-lightbox="prt_5" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК Тау Шатыры в процессе/10.jpeg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Дизайн холла Патифон</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/1.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/1.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/2.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/2.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/3.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/3.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/4.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/4.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/5.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/5.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/6.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/6.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/7.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/7.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/Дизайн холла Патифон/8.jpg" data-lightbox="prt_6" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Дизайн холла Патифон/8.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 7 -->
    <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>ЖК Тау Шатыры</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 8 -->
    <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Квартира Толе Би Тау</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 9 -->
    <div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Офис 3d визуализация</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/1.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/2.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/2.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/3.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/3.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/4.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/4.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/5.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/5.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/6.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/6.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/7.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/7.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/8.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/8.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/9.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/9.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/10.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/10.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/11.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/11.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/12.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/12.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/13.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/13.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/14.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/14.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/15.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/15.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/16.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/16.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/17.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/17.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/18.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/18.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/19.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/19.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/19.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/20.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/20.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/21.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/21.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/22.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/22.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/23.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/23.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/24.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/24.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/25.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/25.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/26.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/26.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/27.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/27.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/28.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/28.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/29.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/29.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/30.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/30.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/31.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/31.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Офис 3d визуализация/32.jpg" data-lightbox="prt_9" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Офис 3d визуализация/32.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 10 -->
    <div class="portfolio-modal modal fade" id="portfolioModal10" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Ресторан Лотос</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/1.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/1.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/2.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/2.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/3.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/3.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/4.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/4.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/5.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/5.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/6.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/6.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лотос/7.jpeg" data-lightbox="prt_10" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лотос/7.jpeg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/1.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/1.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/2.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/2.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/3.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/3.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/4.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/4.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/5.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/5.jpg" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Лотос/6.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/6.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/7.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/7.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/8.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/8.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/9.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/9.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/10.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/10.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Лотос/11.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/11.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Лотос/12.jpg" data-lightbox="prt_20" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Лотос/12.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 11 -->
    <div class="portfolio-modal modal fade" id="portfolioModal11" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Ресторан Лофт</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/1.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/1.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/2.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/2.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/3.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/3.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/4.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/4.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/5.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/5.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/6.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/6.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/7.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/7.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/8.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/8.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/9.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/9.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/10.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/10.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/11.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/11.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/12.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/12.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/13.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/13.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/14.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/14.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/15.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/15.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/16.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/16.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/17.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/17.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/18.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/18.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/19.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/19.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/20.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/20.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/21.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/21.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/22.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/22.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/23.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/23.jpeg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Ресторан Лофт алматы/24.jpeg" data-lightbox="prt_11" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Ресторан Лофт алматы/24.jpeg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 12 -->
    <div class="portfolio-modal modal fade" id="portfolioModal12" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Салон Красоты Мокка</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/2.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/2.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/3.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/3.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/4.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/4.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/5.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/5.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/6.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/6.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/7.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/7.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/8.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/8.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/9.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/9.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/10.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/10.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/11.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/11.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/12.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/12.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" data-lightbox="prt_12" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/Салон Красоты Мокка г Астана/1.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 13 -->
    <div class="portfolio-modal modal fade" id="portfolioModal13" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>ТАУ САМАЛ</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/3.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/3.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/4.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/4.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/5.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/5.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/6.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/6.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/7.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/7.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/8.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/8.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/9.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/9.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/10.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/10.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/11.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/11.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/12.png" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/12.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/13.jpg" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/13.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/1.jpg" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/1.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ТАУ САМАЛ/2.jpg" data-lightbox="prt_13" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ТАУ САМАЛ/2.jpg" alt="">
                                </a>

                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 15 -->
    <div class="portfolio-modal modal fade" id="portfolioModal15" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Офиса Курмангазы Панфилова</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                            
                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/1.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/2.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/2.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/3.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/3.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/4.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/4.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/5.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/5.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/6.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/6.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/7.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/7.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/8.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/8.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/9.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/9.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/10.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/10.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/11.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/11.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/12.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/12.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/13.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/13.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/14.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/14.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/15.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/15.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/16.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/16.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/17.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/17.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/18.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/18.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/19.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/19.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/20.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/20.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/21.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/21.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/22.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/22.jpg" alt="">
                                </a>

                                
                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/23.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/23.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/24.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/24.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/фото Офиса Курмангазы Панфилова/25.jpg" data-lightbox="prt_15" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/фото Офиса Курмангазы Панфилова/25.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 16 -->
    <div class="portfolio-modal modal fade" id="portfolioModal16" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2></h2>
                            <p class="item-intro text-muted">Загородный дом</p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/gulnara/Загородный дом/1.jpg" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Загородный дом/1.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Загородный дом/2.jpg" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Загородный дом/2.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Загородный дом/3.png" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Загородный дом/3.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Загородный дом/4.png" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Загородный дом/4.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Загородный дом/5.png" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Загородный дом/5.png" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Загородный дом/6.png" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Загородный дом/6.png" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 17 -->
    <div class="portfolio-modal modal fade" id="portfolioModal17" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Нурлы-Тау</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/1.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/1.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/2.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/2.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/3.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/3.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/4.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/4.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/5.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/5.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/6.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/6.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/6.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/6.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/7.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/7.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/8.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/8.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/9.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/9.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/10.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/10.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/11.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/11.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/12.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/12.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/13.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/13.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/14.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/14.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/15.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/15.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау/16.jpg" data-lightbox="prt_17" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау/16.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio Modal 18 -->
    <div class="portfolio-modal modal fade" id="portfolioModal18" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Нурлы-Тау</h2>
                            <p class="item-intro text-muted"></p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/1.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/1.png" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/2.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/2.png" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/3.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/3.png" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/4.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/4.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/5.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/5.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/6.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/6.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/7.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/7.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/8.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/8.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/9.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/1 этаж офис/9.jpg" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/1.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/1.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/2.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/2.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/3.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/3.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/4.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/4.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/5.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/5.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/6.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/6.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/7.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/7.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/8.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/8.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/9.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/9.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/10.png" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/10.png" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/11.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/11.jpg" alt="">
                                </a>


                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/12.jpg" data-lightbox="prt_18" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау2/2 этаж офис/12.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!-- Portfolio Modal 19 -->
    <div class="portfolio-modal modal fade" id="portfolioModal19" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2></h2>
                            <p class="item-intro text-muted">ЖК Радуга</p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/1.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/1.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/2.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/2.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/3.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/3.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/4.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/4.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/5.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/5.jpg" alt="">
                                </a>

                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/6.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/6.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/7.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/7.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/8.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/8.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/9.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/9.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/anel/ЖК РАДУГА/10.jpg" data-lightbox="prt_19" data-title="">
                                    <img class="port_img" src="img/portfolio/anel/ЖК РАДУГА/10.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Portfolio Modal 20 -->
    <div class="portfolio-modal modal fade" id="portfolioModal20" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2></h2>
                            <p class="item-intro text-muted">Лотос</p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Portfolio Modal 21 -->
    <div class="portfolio-modal modal fade" id="portfolioModal21" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2></h2>
                            <p class="item-intro text-muted">Нурлы Тау</p>
                            <ul class="list-inline">
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>

                            <div class="gallery_portflio">
                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау3/1.jpg" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау3/1.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау3/2.jpg" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау3/2.jpg" alt="">
                                </a>
                                <a class="port_a" href="img/portfolio/gulnara/Нурлы-Тау3/3.jpg" data-lightbox="prt_16" data-title="">
                                    <img class="port_img" src="img/portfolio/gulnara/Нурлы-Тау3/3.jpg" alt="">
                                </a>
                            </div>

                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- jQuery -->
    <script src="js/jquery.js"></script>



    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>


    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/lightbox.js"></script>
    <script src="js/agency.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32039341 = new Ya.Metrika({
                    id:32039341,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32039341" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>

</html>
