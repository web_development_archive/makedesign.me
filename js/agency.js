/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

var works_cirle = false;
var num_1 = false;
var num_2 = false;
var num_3 = false;
var num_4 = false;
var num_5 = false;
var num_6 = false;
var num_7 = false;
var num_8 = false;

function viewcheck(num, count)
{
    v_num = isElementVisible("#num_"+num);
    if (v_num) {
        switch(num)
        {
            case 1:
                num = num_1;
            break;
            case 2:
                num = num_2;
            break;
            case 3:
                num = num_3;
            break;
            case 4:
                num = num_4;
            break;
            case 5:
                num = num_5;
            break;
            case 6:
                num = num_6;
            break;
            case 7:
                num = num_7;
            break;
            case 8:
                num = num_8;
            break;
        }
        if (num == false)
            $({
                someValue: 0
            }).animate({
                someValue: count
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $("#num_"+num).text(commaSeparateNumber(Math.round(this.someValue)));
                    switch(num)
                    {
                        case 1:
                            num_1 = true;
                        break;
                        case 2:
                            num_2 = true;
                        break;
                        case 3:
                            num_3 = true;
                        break;
                        case 4:
                            num_4 = true;
                        break;
                        case 5:
                            num_5 = true;
                        break;
                        case 6:
                            num_6 = true;
                        break;
                        case 7:
                            num_7 = true;
                        break;
                        case 8:
                            num_8 = true;
                        break;
                    }
                }
            });
    }
}

$( window ).scroll(function() {



    v_num_1 = isElementVisible("#num_1");
    if(num_1==false)    { if(v_num_1)
            $({someValue: 0}).animate({someValue: 11}, {duration: 4000,easing:'swing',step: function() {
                  $('#num_1').text(commaSeparateNumber(Math.round(this.someValue))); num_1 = true; } });}
    
    v_num_2 = isElementVisible("#num_2");
    if (num_2 == false) {
        if (v_num_2)
            $({
                someValue: 0
            }).animate({
                someValue: 86
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_2').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_2 = true;
                }
            });
    }
    v_num_3 = isElementVisible("#num_3");
    if (num_3 == false) {
        if (v_num_3)
            $({
                someValue: 0
            }).animate({
                someValue: 150
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_3').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_3 = true;
                }
            });
    }
    v_num_4 = isElementVisible("#num_4");
    if (num_4 == false) {
        if (v_num_4)
            $({
                someValue: 0
            }).animate({
                someValue: 27
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_4').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_4 = true;
                }
            });
    }

    v_num_5 = isElementVisible("#num_5");
    if (num_5 == false) {
        if (v_num_5)
            $({
                someValue: 0
            }).animate({
                someValue: 2
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_5').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_5 = true;
                }
            });
    }

    v_num_6 = isElementVisible("#num_6");
    if (num_6 == false) {
        if (v_num_6)
            $({
                someValue: 0
            }).animate({
                someValue: 11
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_6').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_6 = true;
                }
            });
    }

    v_num_7 = isElementVisible("#num_7");
    if (num_7 == false) {
        if (v_num_7)
            $({
                someValue: 0
            }).animate({
                someValue: 3
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_7').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_7 = true;
                }
            });
    }

    v_num_8 = isElementVisible("#num_8");
    if (num_8 == false) {
        if (v_num_8)
            $({
                someValue: 0
            }).animate({
                someValue: 13
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_8').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_8 = true;
                }
            });
    }

    v_works_cirle = isElementVisible(".works_all");
    if(works_cirle==false)    { 
        if(v_works_cirle)
        {
            //alert(1);
            
            /*
            
            $(".arrow_right").fadeIn(3000);
            $(".arrow_bottom").fadeIn(4000);
            $(".arrow_left").fadeIn(5000);
            $(".arrow").fadeIn(6000);
            */
            $(".arrow_right").show("slide", { direction: "left"}, 3000);
            $(".arrow_bottom").show("slide", { direction: "bottom" }, 3000);
            $(".arrow_left").show("slide", { direction: "right" }, 3000);
            $(".arrow").show("slide", 4000);
            works_cirle = true;
        }
    }

            
});

// jQuery for page scrolling feature - requires jQuery Easing plugin

$(function() {
//$("#call_phone").mask("+7 (999) 999-9999");


    
    

    $(".faktsBG").mouseenter(function() {
        
    if(num_1==false)    { 
            $({someValue: 0}).animate({someValue: 11}, {duration: 4000,easing:'swing',step: function() {
                  $('#num_1').text(commaSeparateNumber(Math.round(this.someValue))); num_1 = true; } });}
    
    if (num_2 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 86
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_2').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_2 = true;
                }
            });
    }
    if (num_3 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 150
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_3').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_3 = true;
                }
            });
    }
    if (num_4 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 27
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_4').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_4 = true;
                }
            });
    }

    if (num_5 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 2
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_5').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_5 = true;
                }
            });
    }

    if (num_6 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 11
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_6').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_6 = true;
                }
            });
    }

    if (num_7 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 3
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_7').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_7 = true;
                }
            });
    }

    if (num_8 == false) {
            $({
                someValue: 0
            }).animate({
                someValue: 13
            }, {
                duration: 3000,
                easing: 'swing',
                step: function() {
                    $('#num_8').text(commaSeparateNumber(Math.round(this.someValue)));
                    num_8 = true;
                }
            });
    }
  });


	var set_slide = function (selector, slide) {
        $(selector).each(function () {
            var position = slide * 400 + 300;
            var block = $(this);
            var shift = function () {
                block.css('background-position', '0 ' + position + 'px');
                position -= 100;
            }
            setTimeout(shift, 60);
            setTimeout(shift, 120);
            setTimeout(shift, 180);
            setTimeout(shift, 240);
        });
    }

    var time = new Date();
    var target_time = new Date(time.getFullYear(), time.getMonth(), time.getDate());
    target_time = target_time.valueOf() + 1000 * 60 * 60 * 24;

    var tick = function (init) {
        if (init == undefined) {
            init = false;
        }
        var current_time = new Date();
        current_time = current_time.valueOf();
        if (current_time > target_time) {
            var time = new Date();
            target_time = new Date(time.getFullYear(), time.getMonth(), time.getDate());
            target_time = target_time.valueOf() + 1000 * 60 * 60 * 24;
        }
        var time_diff = Math.floor((target_time - current_time) / 1000);
        var second_2 = time_diff % 10;
        time_diff = Math.floor(time_diff / 10);
        var second_1 = time_diff % 6;
        time_diff = Math.floor(time_diff / 6);
        var minute_2 = time_diff % 10;
        time_diff = Math.floor(time_diff / 10);
        var minute_1 = time_diff % 6;
        time_diff = Math.floor(time_diff / 6);
        var hour_2 = Math.floor(time_diff / 10);
        var hour_1 = time_diff % 10;
        set_slide('div.secondPlay', second_2);
        if ((second_2 == 9) || init) {
            set_slide('div.second6Play', second_1);
            if ((second_1 == 5) || init) {
                set_slide('div.minutePlay', minute_2);
                if ((minute_2 == 9) || init) {
                    set_slide('div.minute6Play', minute_1);
                    if ((minute_1 == 5) || init) {
                        set_slide('div.hourPlay', hour_1);
                        if ((hour_2 == 9) || init) {
                            set_slide('div.hour2Play', hour_2);
                        }
                    }
                }
            }
        }
    }
    tick(true);
    setInterval(tick, 1000);
	$('#submit_call').on('click', function() {
        zakaz_zvonka();
    });

    $('.zvonok_footer').on('click', function() {
        $('.popup, .overlay').css('opacity','1');
        $('.popup, .overlay').css('visibility','visible');
        e.preventDefault();
    });

    $('#zazak_header').on('click', function() {
        $('.popup, .overlay').css('opacity','1');
        $('.popup, .overlay').css('visibility','visible');
        e.preventDefault();
    });

	$('.popup .close_window, .overlay').click(function (){
        $('.popup, .overlay').css('opacity','0');
        $('.popup, .overlay').css('visibility','hidden');
    });
    $('.phone_ring').click(function (e){
        $('.popup, .overlay').css('opacity','1');
        $('.popup, .overlay').css('visibility','visible');
        e.preventDefault();
    });


    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    $('#sbmBtnCntr').on('click', function() {
        zakaz_zvonka2();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

function zakaz_zvonka() {
    var name = $('#call_name').val();
    var phone = $('#call_phone').val();

//    var abonent = phone.substring(4, 7);
  //  var tri = phone.substring(9,12);
//    var chetyre = phone.substring(13,19);
    //phone = 8 + abonent + tri + chetyre;

    $.ajax({
        type: "POST",
        url: "call.php",
        data: {name:name,phone:phone},
        cache: false,
        success: function (data) {
            //alert(data);
            var succ_res = '<div id="parent_popup"><div id="myblock" class="myClass"><span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer" id="fasfas"><a id="open-close">x</a></span><div style="width: 300px; margin: auto;"><p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Спасибо!</p><p style="text-align: center; font-size: 18px; line-height: 0;">Ваш заказ был принят.</p></div></div><div class="clear"></div></div>';
            $('.popup, .overlay').css('opacity','0');
            $('.popup, .overlay').css('visibility','hidden');
            $('body').prepend(succ_res);
            bbb();
        }
    });
}

function zakaz_zvonka2() {
    var name = $('#salename').val();
    var email = $('#saleemail').val();
    var phone = $('#salephone').val();

    $.ajax({
        type: "POST",
        url: "call2.php",
        data: {name:name,phone:phone,email:email},
        cache: false,
        success: function (data) {
            //alert(data);
            var succ_res = '<div id="parent_popup"><div id="myblock" class="myClass"><span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer" id="fasfas"><a id="open-close">x</a></span><div style="width: 300px; margin: auto;"><p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Спасибо!</p><p style="text-align: center; font-size: 18px; line-height: 0;">Ваш заказ был принят.</p></div></div><div class="clear"></div></div>';
            $('.popup, .overlay').css('opacity','0');
            $('.popup, .overlay').css('visibility','hidden');
            $('body').prepend(succ_res);
            bbb();
        }
    });
}

function isElementVisible(elementToBeChecked) {
    var TopView = $(window).scrollTop();
    var BotView = TopView + $(window).height();
    var TopElement = $(elementToBeChecked).offset().top;
    var BotElement = TopElement + $(elementToBeChecked).height();
    return ((BotElement <= BotView) && (TopElement >= TopView));  
}


 function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    return val;
  }

  function bbb()
  {
    $('#open-close').click(function(event) {
        event.preventDefault();
        $('#myblock').slideToggle();
        document.getElementById('parent_popup').style.display='none';
        $('#call_name, #call_phone').val("");
        $('#salename, #salephone, #saleemail').val("");

        $('#unsuccess').css('display', 'none');
        $('.popup, .overlay').css('opacity','0');
        $('.popup, .overlay').css('visibility','hidden');
        
    });
  }